#!/usr/bin/env bash
profile=user
echo profile: $profile
if [ $profile == 'admin' ]
then
    user=vik-admin
    password=admin
else
    user=vik-user
    password=user
fi
realm=backend-service
client=token-backend-client

#script to get access token
    export TOKEN=`curl -ss  --data "grant_type=password&client_id=$client&username=$user&password=$password" \
        http://localhost:9001/auth/realms/$realm/protocol/openid-connect/token | jq -r .access_token`

echo TOKEN:$TOKEN

if [ $profile == 'admin' ]
then
    curl -ss -H "Authorization: bearer $TOKEN" http://localhost:9002/admin/hello
else
    curl -ss -H "Authorization: bearer $TOKEN" http://localhost:9002/user/hello
fi
